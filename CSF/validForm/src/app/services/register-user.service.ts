import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';

import { RegisterUser } from '../shared/register-user';

const httpOptions = {
  headers: new HttpHeaders( {'Content-Type':'application/json'} )
}

@Injectable()
export class RegisterUserService {
  private userAPIURL = `${environment.backendApiURL}/api/user`

  constructor(private httpClient: HttpClient) { }

  public saveUser(user: RegisterUser): Observable<RegisterUser> {
    return this.httpClient.post<RegisterUser>(this.userAPIURL + 'register', user, httpOptions);
  }


} // injectable
