import { Component, OnInit } from '@angular/core';
import { RegisterUser } from './shared/register-user';
import { RegisterUserService } from './services/register-user.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'app';
  model = null;
  gender: string[] = ['M', 'F'];
  
  countries = [ {desc:'Singapore', value:'SG'}, 
                {desc: 'Malaysia', value:'MY'}, 
                {desc:'Thailand', value:'TH'},  
                {desc:'Vietnam', value: 'VN'}];
                    
  isSubmitted: boolean = false;
  todayDate: any = Date.now();
  
constructor(private userService: RegisterUserService) {
} // constructor

ngOnInit() {
  this.model = new RegisterUser('email','pw','Tan','F',null,'add','SG','666',0);
} // ngOnInit

onSubmit() {
  this.model.age = this.calAge();
  console.log(this.model.email);
  console.log(this.model.password);
  console.log(this.model.name);
  console.log(this.model.gender);
  console.log(this.model.dob);
  console.log(this.model.address);
  console.log(this.model.country);
  console.log(this.model.contactNum);
  console.log(this.model.age);

  this.userService.saveUser(this.model).subscribe(user => {
      console.log('send to backend !');
      console.log(user);
      this.model = user;
    })
  this.isSubmitted = true;
} // onSubmit

public calAge()  {
  
  return 18;
}

} // appComponent Class


