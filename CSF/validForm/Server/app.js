/**
 * Server side app
 */

console.log("Starting server side app ...");
/* start : import libs */
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
/* end : import libs */

var app = express();
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(cors());
console.log(__dirname);
const NODE_PORT = process.env.PORT;

app.use(express.static(__dirname + "/../dist/"));

app.post("/api/user/register", (req, res)=>{
   console.log(req);
   var user = req.body;
   console.log(user);   
   res.status(200).json(user);  // passing back the response
});

app.listen(NODE_PORT, function(){
    console.log(`Backend Server started at ${NODE_PORT}`);
})