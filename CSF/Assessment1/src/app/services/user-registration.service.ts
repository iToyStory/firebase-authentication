import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs/Observable';

import { RegistrationUser } from '../shared/registration-user';
//import { ProductReview } from '../shared/product-review';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class UserRegistrationService {
  private userApiURL = `${environment.backendApiURL}/api/user/` 
  
  constructor(private httpClient: HttpClient) { }


  public saveUserRegistration(user: RegistrationUser) : Observable<RegistrationUser>{
    return this.httpClient.post<RegistrationUser>(this.userApiURL + 'register', user, httpOptions);
  }

}