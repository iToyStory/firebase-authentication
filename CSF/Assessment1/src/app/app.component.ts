import { Component, OnInit } from '@angular/core';
import { RegistrationUser } from './shared/registration-user';
import { UserRegistrationService } from './services/user-registration.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'app';
  model = null;
  gender: string[] = ['M', 'F'];
  
  countries = [ {desc:'Singapore', value:'SG'}, 
                {desc:'Malaysia', value:'MY'}, 
                {desc:'Thailand', value:'TH'},  
                {desc:'Vietnam', value: 'VN'}];
                    
  isSubmitted: boolean = false;
  result: string = "";
  isAbove18: boolean = false;
  dobDate: Date = new Date();
  currentDate: Date = new Date();

  constructor(private userService: UserRegistrationService){
  } //constructor
  
  // when the page load
  ngOnInit() {
    this.model = new RegistrationUser('','','','',null,'','SG','',0);
  } //ngOnoInit
  
  // this is to handle when I click on a submit or save button.
  onSubmit(){
    console.log(this.model.email);
    console.log(this.model.password);
    console.log(this.model.confirmPassword);
    console.log(this.model.firstName);
    console.log(this.model.lastName);
    console.log(this.model.gender);
    console.log(this.model.dateOfBirth);
    console.log(this.model.address);
    console.log(this.model.country);
    console.log(this.model.contactNum);
  
    this.userService.saveUserRegistration(this.model)
      .subscribe(user => {
        console.log('send to backend !');
        console.log(user);
        this.model = user;
      })
    this.isSubmitted = true;   
  } // onSubmit

  onBack() {
    this.isSubmitted =false;
  } //onBack


  updateAge(event) {
    //console.log(this.currentDate);
    /*var nowValue = JSON.stringify(this.currentDate).valueOf();
    console.log(nowValue);
    var dobValue = event.valueOf();
    var calAge = nowValue - calAge;
    console.log(dobValue);
    console.log(calAge);
    console.log(this.model.age<18);
    console.log(this.model.age>17);*/
    this.model.age = 20;  // cannot calculate the age, so fake an age to test the logic
  }

} // AppComponent