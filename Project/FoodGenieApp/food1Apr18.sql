-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: food
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alacart_dish`
--

DROP TABLE IF EXISTS `alacart_dish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alacart_dish` (
  `dish_id` int(11) NOT NULL AUTO_INCREMENT,
  `dish_name` varchar(300) NOT NULL,
  `descriptions` varchar(1000) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `unit` enum('pax','pc','tray') NOT NULL,
  `dish_rank` smallint(2) NOT NULL,
  `vegetarian` enum('y','n') NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL DEFAULT '1',
  `is_obsolete` tinyint(1) NOT NULL DEFAULT '0',
  `obsolete_date` timestamp NULL DEFAULT NULL,
  `order_side_id` int(11) DEFAULT NULL COMMENT 'A alacart dish may or may not belong to an order side',
  PRIMARY KEY (`dish_id`),
  UNIQUE KEY `dish_id_UNIQUE` (`dish_id`),
  KEY `fk_menuUser_idx` (`updated_by`),
  KEY `fk_dish_side_order_idx` (`order_side_id`),
  KEY `fk_dish_rank_idx` (`dish_rank`),
  CONSTRAINT `fk_dish_rank` FOREIGN KEY (`dish_rank`) REFERENCES `dish_type` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dish_side_order` FOREIGN KEY (`order_side_id`) REFERENCES `order_side` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dish_user` FOREIGN KEY (`updated_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alacart_dish`
--

LOCK TABLES `alacart_dish` WRITE;
/*!40000 ALTER TABLE `alacart_dish` DISABLE KEYS */;
INSERT INTO `alacart_dish` VALUES (21,'teriyaki salmon','this is japanese fish',3.00,'pax',4,'n','2018-03-24 20:51:54',1,0,NULL,NULL),(22,'salsa chicken','this is western chicken',2.00,'pax',6,'n','2018-03-24 20:52:41',1,0,NULL,NULL),(23,'capsicum omelete','this is western omelete',1.50,'pax',8,'n','2018-03-24 20:53:38',1,0,NULL,NULL),(24,'calamari ring','this is western seafood',2.00,'pax',3,'n','2018-03-24 20:54:15',1,0,NULL,NULL),(25,'potato croquette','this is japanese potato',2.00,'pax',8,'n','2018-03-24 20:55:09',1,0,NULL,NULL),(26,'fruit tartlet','this is western bakery',1.50,'pax',9,'n','2018-03-24 20:56:13',1,0,NULL,NULL),(27,'panna cotta','this is western dessert',1.50,'pax',9,'n','2018-03-24 20:56:37',1,0,NULL,NULL),(28,'orange juice','this is western dessert',2.00,'pax',11,'y','2018-03-24 20:57:32',1,0,NULL,NULL),(29,'mesclun salad','this is western appetiser',1.50,'pax',1,'y','2018-03-28 10:51:29',1,0,NULL,NULL),(30,'fruit salad','this is western appetiser',1.50,'pax',1,'y','2018-03-28 10:51:47',1,0,NULL,NULL),(31,'yellow rice','this is malay rice',1.50,'pax',2,'y','2018-03-28 10:52:52',1,0,NULL,NULL),(32,'breaded fish','this is western fish',2.00,'pax',4,'n','2018-03-28 10:55:22',1,0,NULL,NULL),(33,'rendang mutton','this is malay mutton',3.00,'pax',5,'n','2018-03-28 10:56:04',1,0,NULL,NULL),(34,'roast vegetables','this is western vegetables',1.50,'pax',7,'n','2018-03-28 11:28:01',1,0,NULL,NULL),(35,'sambal goreng','this is malay vegetables',1.50,'pax',7,'n','2018-03-28 11:28:21',1,0,NULL,NULL),(36,'cheng tng','this is chinese dessert',1.50,'pax',10,'y','2018-03-28 11:29:25',1,0,NULL,NULL),(37,'achar','this is malay salad',1.50,'pax',1,'y','2018-03-29 05:23:53',1,0,NULL,NULL),(38,'braised duck','this is chinese duck',3.00,'pax',5,'n','2018-03-29 05:25:02',1,0,NULL,NULL),(39,'fruit punch','this is chilled beverage',1.00,'pax',11,'y','2018-03-29 05:26:37',1,0,NULL,NULL),(40,'butter rice','this is butter rice',1.50,'pax',2,'n','2018-03-29 12:11:16',1,0,NULL,NULL),(41,'coleslaw','this is western salad',1.50,'pax',1,'y','2018-03-29 13:47:25',1,0,NULL,NULL),(42,'green papaya select','this is thai salad',1.50,'pax',1,'y','2018-03-29 14:01:44',1,0,NULL,NULL),(43,'tomato spaghetti','this is western staple',1.50,'pax',2,'y','2018-03-29 14:03:03',1,0,NULL,NULL),(44,'sugarcane prawn','this is thai prawn',3.00,'pax',3,'n','2018-03-29 14:04:00',1,0,NULL,NULL),(45,'cheese baked prawn','this is western prawn',3.00,'pax',3,'n','2018-03-29 14:04:49',1,0,NULL,NULL),(46,'sambal prawn','this is malay prawn',3.00,'pax',3,'n','2018-03-29 14:05:26',1,0,NULL,NULL),(47,'steam fish','this is chinese fish',2.00,'pax',4,'n','2018-03-29 14:09:32',1,0,NULL,NULL),(48,'assam fish','this is malay fish',2.00,'pax',4,'n','2018-03-29 14:09:53',1,0,NULL,NULL),(49,'pesto salmon','this is western fish',2.00,'pax',4,'n','2018-03-29 14:10:44',1,0,NULL,NULL),(50,'xing zhou fried bee hoon','chinese staple',1.50,'pax',2,'y','2018-03-29 14:28:20',1,0,NULL,NULL),(51,'rendang chicken','this is malay chicken',2.00,'pax',6,'n','2018-03-29 14:32:06',1,0,NULL,NULL),(52,'chicken cutlet','this is western chicken',2.00,'pax',6,'n','2018-03-29 14:32:36',1,0,NULL,NULL),(53,'five-spice roast chicken','this is chinese chicken',2.00,'pax',6,'n','2018-03-29 14:33:30',1,0,NULL,NULL),(54,'curry chicken','this is chinese chicken',2.00,'pax',6,'n','2018-03-29 14:34:10',1,0,NULL,NULL),(55,'rendang beef','this is malay beef',3.00,'pax',5,'n','2018-03-29 14:34:47',1,0,NULL,NULL),(56,'masala mutton','this is indian mutton',3.00,'pax',5,'n','2018-03-29 14:35:21',1,0,NULL,NULL),(57,'broccoli with oyster sauce','this is chinese vegetable',1.50,'pax',7,'n','2018-03-29 14:36:12',1,0,NULL,NULL),(58,'butter cauliflower','this is western vegetable',1.50,'pax',7,'n','2018-03-29 14:37:14',1,0,NULL,NULL),(59,'fried fish ball','this is all time favourite',1.00,'pax',8,'n','2018-03-29 14:38:39',1,0,NULL,NULL),(60,'fried sotong ball','this is party favourite',1.00,'pax',8,'n','2018-03-29 14:39:05',1,0,NULL,NULL),(61,'nyonya kueh','this is kueh kueh',1.00,'pax',9,'n','2018-03-29 14:41:03',1,0,NULL,NULL),(62,'cream puff','this is western bakes',1.00,'pax',9,'n','2018-03-29 14:43:57',1,0,NULL,NULL),(63,'chocolate eclair','this is western bakes',1.00,'pax',9,'n','2018-03-29 14:44:20',1,0,NULL,NULL),(64,'red bean','chinese dessert',1.00,'pax',10,'n','2018-03-29 14:44:53',1,0,NULL,NULL),(65,'fruit cocktail','light dessert',1.00,'pax',10,'n','2018-03-29 14:45:56',1,0,NULL,NULL),(66,'mango with sticky rice','thai dessert',2.00,'pax',10,'n','2018-03-29 14:46:30',1,0,NULL,NULL),(67,'coffee','hot beverage',1.00,'pax',11,'n','2018-03-29 14:46:54',1,0,NULL,NULL),(68,'tea','hot beverage',1.00,'pax',11,'n','2018-03-29 14:47:20',1,0,NULL,NULL),(69,'lemongrass drinks','thai drinks',1.00,'pax',11,'n','2018-03-29 14:47:41',1,0,NULL,NULL),(70,'breaded prawn','this is western prawn',3.00,'pax',3,'n','2018-04-01 13:32:29',1,0,NULL,NULL);
/*!40000 ALTER TABLE `alacart_dish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buffet_dish_details`
--

DROP TABLE IF EXISTS `buffet_dish_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buffet_dish_details` (
  `buffet_id` int(11) NOT NULL,
  `dish_id` int(11) NOT NULL,
  KEY `fk_details_dish_idx` (`dish_id`),
  KEY `fk_details_buffet_idx` (`buffet_id`),
  CONSTRAINT `fk_details_buffet` FOREIGN KEY (`buffet_id`) REFERENCES `buffet_set` (`buffet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_details_dish` FOREIGN KEY (`dish_id`) REFERENCES `alacart_dish` (`dish_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buffet_dish_details`
--

LOCK TABLES `buffet_dish_details` WRITE;
/*!40000 ALTER TABLE `buffet_dish_details` DISABLE KEYS */;
INSERT INTO `buffet_dish_details` VALUES (3,31),(3,44),(3,21),(3,22),(3,23),(3,25),(3,26),(3,27),(3,28),(4,40),(4,45),(4,32),(4,52),(4,34),(4,65),(4,27),(4,28);
/*!40000 ALTER TABLE `buffet_dish_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buffet_set`
--

DROP TABLE IF EXISTS `buffet_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buffet_set` (
  `buffet_id` int(11) NOT NULL AUTO_INCREMENT,
  `buffet_name` varchar(300) NOT NULL,
  `descriptions` varchar(300) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `unit` enum('pax','set') NOT NULL COMMENT 'part set - unit is per set\nBuffet - unit is per pax\n',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL DEFAULT '1',
  `is_obsolete` tinyint(1) NOT NULL DEFAULT '0',
  `obsolete_date` timestamp NULL DEFAULT NULL,
  `order_set_id` int(11) DEFAULT NULL COMMENT 'a buffet may or may not belong to an order set',
  PRIMARY KEY (`buffet_id`),
  KEY `fk_buffet_order_set_idx` (`order_set_id`),
  KEY `fk_buffet_user_idx` (`updated_by`),
  CONSTRAINT `fk_buffet_order_set` FOREIGN KEY (`order_set_id`) REFERENCES `order_set` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_buffet_user` FOREIGN KEY (`updated_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buffet_set`
--

LOCK TABLES `buffet_set` WRITE;
/*!40000 ALTER TABLE `buffet_set` DISABLE KEYS */;
INSERT INTO `buffet_set` VALUES (3,'international buffet','this menu is international food!',18.00,'pax','2018-03-31 22:58:11',1,0,NULL,NULL),(4,'western buffet','this menu is western food!',22.00,'pax','2018-03-28 11:29:25',1,0,NULL,NULL),(6,'sunflower','this is sunflower',5.00,'pax','2018-04-01 04:35:44',1,0,NULL,NULL),(7,'daisy','this is daisy',8.00,'pax','2018-04-01 04:52:40',1,0,NULL,NULL),(9,'super value','this is super value',7.00,'pax','2018-04-01 10:45:03',1,0,NULL,NULL),(13,'lunch set','the best lunch set',3.00,'pax','2018-04-01 11:50:57',1,0,NULL,NULL);
/*!40000 ALTER TABLE `buffet_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charge_details`
--

DROP TABLE IF EXISTS `charge_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charge_details` (
  `charge_id` int(11) NOT NULL,
  `charge_type` varchar(100) NOT NULL COMMENT 'Transport\nMorning surcharge\nSurcharge for venue without lift landing\nBooth setup\nService staff\nLate collection',
  `price` decimal(6,2) NOT NULL,
  PRIMARY KEY (`charge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charge_details`
--

LOCK TABLES `charge_details` WRITE;
/*!40000 ALTER TABLE `charge_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `charge_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `mobile_number` int(8) NOT NULL,
  `other_contact` varchar(20) DEFAULT NULL,
  `company_name` varchar(300) DEFAULT NULL,
  `billing_address` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (125,'stanley','stanley@email.com',87654321,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_type`
--

DROP TABLE IF EXISTS `dish_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_type` (
  `type_id` smallint(2) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `rank` smallint(2) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_UNIQUE` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_type`
--

LOCK TABLES `dish_type` WRITE;
/*!40000 ALTER TABLE `dish_type` DISABLE KEYS */;
INSERT INTO `dish_type` VALUES (1,'appetiser',1),(2,'staple',2),(3,'seafood',3),(4,'fish',4),(5,'redmeat',5),(6,'chicken',6),(7,'vegetable',7),(8,'side',8),(9,'bakes',9),(10,'dessert',10),(11,'beverage',11);
/*!40000 ALTER TABLE `dish_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `image_id` int(11) NOT NULL,
  `image_url` varchar(300) NOT NULL,
  `buffet_id` int(11) DEFAULT NULL,
  `dish_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `image_id_UNIQUE` (`image_id`),
  KEY `fk_buffet_image_idx` (`buffet_id`),
  KEY `fk_dish_image_idx` (`dish_id`),
  CONSTRAINT `fk_buffet_image` FOREIGN KEY (`buffet_id`) REFERENCES `buffet_set` (`buffet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dish_image` FOREIGN KEY (`dish_id`) REFERENCES `alacart_dish` (`dish_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_set`
--

DROP TABLE IF EXISTS `order_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buffet_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `discount` decimal(6,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_order_set_orders_idx` (`order_id`),
  CONSTRAINT `fk_order_set_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_set`
--

LOCK TABLES `order_set` WRITE;
/*!40000 ALTER TABLE `order_set` DISABLE KEYS */;
INSERT INTO `order_set` VALUES (3,3,30,3,NULL);
/*!40000 ALTER TABLE `order_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_side`
--

DROP TABLE IF EXISTS `order_side`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_side` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dish_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `discount` decimal(6,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_order_side_orders_idx` (`order_id`),
  CONSTRAINT `fk_order_side_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_side`
--

LOCK TABLES `order_side` WRITE;
/*!40000 ALTER TABLE `order_side` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_side` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(45) NOT NULL COMMENT 'allow append revision to order number to track changes',
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `venue_address` varchar(300) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `total_amount` decimal(9,2) NOT NULL,
  `payment_mode` enum('cash','cheque','bank transfer') NOT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `payment_date` date DEFAULT NULL COMMENT 'cancelled order has no payment',
  `order_status` enum('processing','confirmed','delivered with payment','delivered without payment','cancelled') NOT NULL DEFAULT 'processing',
  `created_by` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_order_cust_idx` (`customer_id`),
  KEY `fk_order_user_idx` (`created_by`),
  CONSTRAINT `fk_order_cust` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_user` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (3,'genie/1803/001','2018-03-31 02:55:29','bukit batok west','2018-03-15','11:00:00',540.00,'cash',NULL,NULL,'processing',1,125);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surcharges`
--

DROP TABLE IF EXISTS `surcharges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surcharges` (
  `charge_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `discount` decimal(6,2) DEFAULT NULL,
  KEY `fk_charges_details_idx` (`charge_id`),
  KEY `fk_surcharge_order_idx` (`order_id`),
  CONSTRAINT `fk_surcharge_charge_details` FOREIGN KEY (`charge_id`) REFERENCES `charge_details` (`charge_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_surcharge_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surcharges`
--

LOCK TABLES `surcharges` WRITE;
/*!40000 ALTER TABLE `surcharges` DISABLE KEYS */;
/*!40000 ALTER TABLE `surcharges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Add new user\nUpdate user password\nDelete user',
  `name` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'jane','jane@email.com','Jane$123');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-02  0:56:39
