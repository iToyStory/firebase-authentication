# FoodOrderApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Step 1 Create the Application
cd Project
ng new FoodOrderApp
cd FoodOrderApp

Step 2 Load libraries
npm install bootstrap --save
npm install jquery --save
npm install font-awesome --save
npm install ng2-toasty --save
npm install ngx-bootstrap --save
npm install rxjs --save
npm install core-js --save
npm install popper.js --save
npm install simple-line-icons --save
npm install tether --save
npm install zone.js --save

Step 3 Configure .angular-cli.json  

"styles": [
        "../node_modules/tether/dist/css/tether.min.css",
        "../node_modules/bootstrap/dist/css/bootstrap.min.css",
        "../node_modules/ng2-toasty/bundles/style-bootstrap.css",
        "../node_modules/font-awesome/css/font-awesome.min.css",
        "../node_modules/simple-line-icons/css/simple-line-icons.css",
        "../node_modules/bootstrap/dist/css/bootstrap.min.css",
        "styles.css"
      ],
      "scripts": [
         "../node_modules/jquery/dist/jquery.slim.min.js",
        "../node_modules/tether/dist/js/tether.min.js",
        "../node_modules/popper.js/dist/umd/popper.min.js",
        "../node_modules/bootstrap/dist/js/bootstrap.min.js"],

Step 4 Create Models
ng g class shared/order
ng g class shared/search-criteria
ng g class shared/menu

Step 5 Create services 
ng g service shared/service/order
ng g service shared/service/menu

Step 6 Create Components
ng g c home
ng g c food/search
ng g c food/update
ng g c food/order
ng g c food/menu
ng g c food/menu-details

Step 7 Create Modules and other files
Add shared/shared.module.ts
Add shared/index.ts
Add share/layout/index.ts
Add home/home.module.ts
Add food/food.module.ts
Add food/index.ts

Step 8 Add script to the header of index.html

<link href="//fonts.googleapis.com/css?family=Titillium+Web:700|Source+Serif+Pro:400,700|Merriweather+Sans:400,700|Source+Sans+Pro:400,300,600,700,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
  <link rel="icon" type="image/x-icon" href="favicon.ico">

Step 9 Add css to order.component.css

.has-error {
    border-bottom: 2px solid red;
}

references:
https://getbootstrap.com/docs/4.0/components/collapse/
