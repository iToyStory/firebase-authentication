const CONFIG = {
    host: 'localhost', port: 3306,
    user: 'Fred', password: 'fred',
    database: 'food',
    connectionLimit: 4
};

module.exports = CONFIG;