//load libraries
const express = require('express');
const mysql = require('mysql');
const q = require('q');
const bodyParser = require('body-parser');
const cors = require('cors');

//load config
const mysqlConfig = require('./mysql-config');
const pool = mysql.createPool(mysqlConfig);

const app = express();
//set cors
app.use(cors());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

// **** JOIN MULTIPLE TABLES ****
// get/menus/details/3   ==> need to review, whether to use one big table
// return one menu details with given buffet id
const SELECT_MENU_DETAILS = 'SELECT a.dish_name, a.descriptions, a.vegetarian FROM buffet_set b right JOIN buffet_dish_details d ON b.buffet_id=d.buffet_id JOIN alacart_dish a ON d.dish_id=a.dish_id WHERE b.buffet_id=? ORDER BY dish_rank'
app.get('/menus/details/:id',(req,resp) =>{
    console.log("get/menus/details")
    const id = req.params.id;
    console.log(id)
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_MENU_DETAILS, [ parseInt(id) ],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});
// find all orders of a menu
//get/menus/orders
const SELECT_ORDERS_FOR_MENU = 'SELECT o.order_number, o.event_date FROM orders o JOIN order_set s ON o.order_id = s.order_id JOIN buffet_set b ON b.buffet_id=s.buffet_id where b.buffet_id=?'
app.get('/menus/orders',(req,resp) =>{
    const id = req.query.id;
    console.log("/menus/orders")
    console.log(parseInt(id))
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_ORDERS_FOR_MENU,[parseInt(id)],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
 });

 //get/menus/allId
const SELECT_MENUS_ALL_ID = 'SELECT buffet_id FROM buffet_set ORDER BY buffet_id DESC'
app.get('/menus/allId',(req,resp) =>{
    console.log("get/menus")
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_MENUS_ALL_ID,
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});

// **** GET ALL FUNCTIONS ****
// get/menus/dishes
// return all buffet set menus & respective dishes (one menu has many dishes)
const SELECT_MENUS_DISHES1 = 'SELECT b.buffet_id, b.buffet_name, b.descriptions, b.price, b.unit, a.dish_name, a.vegetarian FROM buffet_set b join buffet_dish_details d on b.buffet_id=d.buffet_id join alacart_dish a on a.dish_id=d.dish_id WHERE b.is_obsolete=0 ORDER BY b.buffet_name, a.dish_rank ASC'
const SELECT_MENUS_DISHES = 'SELECT * FROM buffet_set b join buffet_dish_details d on b.buffet_id=d.buffet_id join alacart_dish a on a.dish_id=d.dish_id'

app.get('/menus/dishes',(req,resp) =>{
    console.log("get/menus/dishes...")
    const obsolete =0 // return only active menus
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_MENUS_DISHES,
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});
// get/menus
// return all buffet set menus
const SELECT_MENUS = 'SELECT * FROM buffet_set WHERE is_obsolete=? ORDER BY buffet_id DESC'
//var FindAllMenus = makeQuery(SELECT_MENUS,pool);
app.get('/menus',(req,resp) =>{
    console.log("get/menus")
    const obsolete =0 // return only active menus

    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_MENUS, [ parseInt(obsolete) ],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});
 
// get/menus/alacart 
// return all alacart dish order by dish rank (ie dish type)
const SELECT_ALACART = 'SELECT * FROM alacart_dish WHERE is_obsolete=? ORDER BY dish_rank ASC'
app.get('/menus/alacart',(req,resp) =>{
    console.log("get/menus/alacart")
    const obsolete =0 // return only active dishes
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        } 
        conn.query(SELECT_ALACART, [parseInt(obsolete)],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
 });

// get/dishtype
// return all dish types
const SELECT_DISH_TYPE = 'SELECT * FROM dish_type ORDER BY rank ASC'
app.get('/dishtype',(req,resp) =>{
    console.log("get/dishtype")
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_DISH_TYPE,
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
 });

// **** INSERT FUNCTIONS ****
const ADD_ALACART = 'INSERT INTO alacart_dish (dish_name,descriptions,price,unit,vegetarian,dish_rank) VALUES (?,?,?,?,?,?)'
app.post('/menus/alacart',
    bodyParser.json(),
    (req,resp) =>{
    const name = req.body.dish_name;
    const desc = req.body.descriptions;
    const price = req.body.price;
    const unit = req.body.unit;
    const veg = req.body.vegetarian;
    const rank = req.body.dish_rank;
    console.log("menus/alacart")
    console.log(req.body);
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(ADD_ALACART,[name, desc, price, unit, veg, rank],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
}); 


// **** INSERT FUNCTIONS ****
// post/menus 
// body {"buffet_name":"western buffet","descriptions":"this menu is western food!","price":22,"unit":"pax"}
// <is_obsolete> has default value 0
// <buffet_id> and <update_date> is system generated
// >> <updated_by> has default value 1, ie user_id 1. This field will be included later.
// >> convert to lowercase
const ADD_BUFFET = 'INSERT INTO buffet_set (buffet_name,descriptions,price,unit) VALUES (?,?,?,?)'
app.post('/menus',
    bodyParser.json(),
    (req,resp) =>{
    const name = req.body.buffet_name;
    const desc = req.body.descriptions;
    const price = req.body.price;
    const unit = req.body.unit;
    console.log("POST/menus, add new menu title ....")
    console.log(name);
    console.log(desc)
    console.log(price)
    console.log(unit)
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(ADD_BUFFET,[name, desc, price, unit],
            (error, result) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(result.insertId);
                } finally {
                    conn.release();
                }
            }
        )
    });
}); 



// post/menus/details
// body {"buffet_id": 16, "dish_id": 24}
const ADD_BUFFET_DETAILS = 'INSERT INTO buffet_dish_details (buffet_id,dish_id) VALUES (?,?)'
app.post('/menus/details',
    bodyParser.json(),
    (req,resp) =>{
    const buffet_id = req.body.buffet_id;
    const dish_id = req.body.dish_id;
    console.log("POST/menus/details, link dishes to set menu ....")

    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(ADD_BUFFET_DETAILS,[buffet_id, dish_id],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
}); 

// **** PUT FUNCTIONS ****
// put/menus
// body {"buffet_name":"international buffet","descriptions":"this menu is international food!","price":19,"buffet_id"=3}
// <updated_by> field has default value 1, ie user_id 1. This field will be included later.
const UPDATE_BUFFET = 'UPDATE buffet_set SET buffet_name=?, descriptions=?, price=? WHERE buffet_id=?'
app.put('/menus',(req,resp) =>{
    const name = req.body.buffet_name;
    const desc = req.body.descriptions;
    const price = req.body.price;
    const id = req.body.buffet_id
    console.log("updating menu title ....")
    console.log(name);
    console.log(desc)
    console.log(price)
    console.log(id)
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(UPDATE_BUFFET,[name, desc, price, id],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
 });


// **** DELETE FUNCTIONS ****
// delete/menus/alacart/1
// <updated_by> field has default value 1, ie user_id 1. This field will be included later.
const DELETE_DISH = 'DELETE FROM alacart_dish WHERE dish_id=?'
app.delete('/menus/alacart/:id',(req,resp) =>{
    const id = req.params.id;
    console.log("deleting dish....")
    console.log(id)
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(DELETE_DISH,[id],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
 });

// delete/menus/details/1
// <updated_by> field has default value 1, ie user_id 1. This field will be included later.
const DELETE_MENU_DETAILS = 'DELETE FROM buffet_dish_details WHERE buffet_id=?'
app.delete('/menus/details/:id',(req,resp) =>{
    const id = req.params.id;
    console.log("removing dishes from set menu....")
    console.log(id)
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(DELETE_MENU_DETAILS,[id],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});

// delete/menus/1
// <updated_by> field has default value 1, ie user_id 1. This field will be included later.
const DELETE_MENU = 'DELETE FROM buffet_set WHERE buffet_id=?'
app.delete('/menus/:id',(req,resp) =>{
    const id = req.params.id;
    console.log("deleting menu title....")
    console.log(id)
    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(DELETE_MENU,[id],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});

//Ping database 
pool.getConnection((err, conn) => {
    if (err) {
        console.error('Database error: ', err);
        process.exit(-1);
    }
    conn.ping((err) => {
        conn.release();
        if (err) {
            console.error('Database error: ', err);
            process.exit(-1);
        }
        const PORT = process.argv[2] || process.env.APP_PORT || 3000;
        app.listen(PORT, () => {
            console.log('Application stared on port %d', PORT);
        });
    })
}) // ping db
