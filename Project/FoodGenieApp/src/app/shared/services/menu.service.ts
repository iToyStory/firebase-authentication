import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import { BuffetSet } from '../../shared/models/buffet-set';
import { AlacartDish } from '../../shared/models/alacart-dish';
import { Order } from '../../shared/models/order';
//import { BuffetDishDetails } from '../../shared/models/buffet-dish-details';

// not necessary to declare as angular always send data in json format
// included anyway for future use
const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}

@Injectable()
export class MenuService {
  private menuData = "/assets/data/menus.json" // for frontend test
  private alacartData = "/assets/data/alacart-dish.json" // for frontend test
  private dishTypeData = "/assets/data/dish-type.json" // for frontend test
  private URL ='http://localhost:3000'

  constructor(
    private httpClient: HttpClient) { }

// **** OTHERS ****
//get/menus/details/3
getMenuDetails(id:string):Promise<any> {
  return (
    this.httpClient.get(`${this.URL}/menus/details/${id}`)
      .take(1).toPromise()
  );
}

// find all orders of a menu
//get/menus/orders/
/* return data
[{
"order_number": "genie/1803/001",
"event_date": "2018-03-14T16:00:00.000Z"
}] 
*/
getOrderForMenu(id:any):Promise<any> { // input is number, output is array of object
  let qs = new HttpParams()
        .set('id',id)
  return (
    this.httpClient.get<any>(`${this.URL}/menus/orders`,{params:qs})
      .take(1).toPromise()
  );
}

// **** GET ALL ****
//get/menus
getAllMenus():Promise<BuffetSet[]> {
  return (
    this.httpClient.get<BuffetSet[]>(`${this.URL}/menus`)
      .take(1).toPromise()
  );
}
getAllMenusId():Promise<string[]> {
  return (
    this.httpClient.get<string[]>(`${this.URL}/menus/allId`)
      .take(1).toPromise()
  );
}
 //get/dishtype
  getDishType():Promise<any> {
    return (
      this.httpClient.get(`${this.URL}/dishtype`)
        .take(1).toPromise()
    );
  }

// get/menus/alacart
getAlacart():Promise<AlacartDish[]> {
  return (
    this.httpClient.get<AlacartDish[]>(`${this.URL}/menus/alacart`)
      .take(1).toPromise()
  );
}

// **** POST ****
// post/menus/alacart
addOneAlacart(dish:AlacartDish):Promise<AlacartDish> {
  console.log(dish)
  return (this.httpClient.post<AlacartDish>(`${this.URL}/menus/alacart`,dish,httpOptions)
  .take(1).toPromise()
  );
}

// post/menus
addOneMenu(menu:BuffetSet):Promise<number> {
  console.log(menu)
  return (this.httpClient.post<number>(`${this.URL}/menus`,menu,httpOptions)
  .take(1).toPromise()
  );
}

// tested ok with numbers
// post/menus/details
addMenuDetails(buffet_id: number, Dish_id:number):Promise<any> {
  return (this.httpClient.post(`${this.URL}/menus/details`,{buffet_id:buffet_id,dish_id:Dish_id},httpOptions)
  .take(1).toPromise()
  );
}

// **** PUT ****
// put/menus
updateOneMenu(menu:BuffetSet):Promise<BuffetSet> {
  console.log(menu)
  return (this.httpClient.put<BuffetSet>(`${this.URL}/menus`,menu,httpOptions)
  .take(1).toPromise()
  );
}

// **** DELETE ****
// delete/menus/alacart/1
deleteOneDish(id:number):Promise<AlacartDish> {
  console.log(id)
  return (this.httpClient.delete<AlacartDish>(`${this.URL}/menus/alacart/${id}`,httpOptions)
  .take(1).toPromise()
  );
}

// delete/menus/1
deleteMenuDetails(id:number):Promise<any> {
  console.log(id)
  return (this.httpClient.delete(`${this.URL}/menus/details/${id}`,httpOptions)
  .take(1).toPromise()
  );
}

// delete/menus/1
deleteOneMenu(id:number):Promise<any> {
  console.log(id)
  return (this.httpClient.delete(`${this.URL}/menus/${id}`,httpOptions)
  .take(1).toPromise()
  );
}








// **** FOR TESTING PURPOSE ONLY ***  
// this method post data to assets/data/menus.json
AddOneMenu1(menu:BuffetSet):Observable<BuffetSet> {
  console.log(menu)
  return this.httpClient.post<BuffetSet>(this.menuData,menu);
}
// this method receive data from assets/data/menus.json
getAllMenus1():Observable<BuffetSet[]> {
  return this.httpClient.get<BuffetSet[]>(this.menuData);
}
// this method receive data from assets/data/alacartData.json
getAlacart1():Observable<AlacartDish[]> {
  return this.httpClient.get<AlacartDish[]>(this.alacartData);
} 
//get/dishtype
getDishType1():Observable<any> {
  return this.httpClient.get<string[]>(this.dishTypeData);
}

}
