export class BuffetSet {
    constructor (
        public buffet_name: string,
        public descriptions: string,
        public price: number,
        public unit: string,
        public updated_by: number,
        public is_obsolete: number,
        public buffet_id?: number, // newly created menu has no buffet_id, its created at server
    ){

    }
}
