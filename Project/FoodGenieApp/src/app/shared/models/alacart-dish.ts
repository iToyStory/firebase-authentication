export class AlacartDish {
    constructor (
        public dish_rank: number,
        public dish_name: string,
        public descriptions: string,
        public price: number,
        public unit: string,
        public vegetarian: string,
        public updated_by: number,
        public is_oboselte: number,
        public dish_id?: number,
    ){

    }
}
