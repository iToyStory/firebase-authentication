import { Time } from "@angular/common";

export class Order {
    constructor (
    public order_number:string,
    public name: string,
    public mobileNumber: string,
    public venue_address: string,
    public event_date: Date,
    public event_time: Time,
    public timeHH: string,
    public timeMM: string,
    public timeAMPM: string,
    public company: string,
    public payment_mode: string,
    public billAddress?: string,
    public otherContact?: string,
    public remarks?: string, 
    public order_id?: number, // new order_id is generated at server
    public orderItem?: {},
){
}
}

