import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuService } from '../../shared/services/menu.service';
import { BuffetSet } from '../../shared/models/buffet-set';
import { AlacartDish } from '../../shared/models/alacart-dish';
import { DatePipe } from '@angular/common';
import { BuffetDishDetails } from '../../shared/models/buffet-dish-details';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  modalRef: BsModalRef; // modal 
  displayMenus: BuffetSet[]; // for loading, store buffet_name, descriptions, price, unit, isObsolete, menuId
  menuItems : string[] = [];  // store buffet details, dish_name, descriptions, vegetarian


  constructor(
    private modalService: BsModalService,
    private menuService: MenuService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

  }

}
