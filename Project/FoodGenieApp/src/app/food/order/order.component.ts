import { Component, OnInit } from '@angular/core';
import { Order } from '../../shared/models/order';
import { BuffetSet } from '../../shared/models/buffet-set';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent implements OnInit {

  orderModel = null;
  menuModel = null;
  reqBillAddr : boolean = false;
  eventType : string = "personal"
  hour: string;
  min: string;


  // event time must be in 15 mins increments 
  HH = [{desc:'1', value:'01'},{desc:'2', value:'02'},{desc:'3', value:'03'},{desc:'4',value:'04'},
          {desc:'5',value:'05'},{desc:'6',value:'06'},{desc:'7',value:'07'},{desc:'8',value:'08'},
          {desc:'9',value:'09'},{desc:'10',value:'10'},{desc:'11',value:'11'},{desc:'12',value:'12'}];
  MM = [{desc:'1', value:'00'},{desc:'2', value:'15'},{desc:'3', value:'30'},{desc:'4',value:'45'}];
  zone: string[] = ['AM','PM'];

  constructor() { }


  ngOnInit() {
    // MM default value 00
    // company default value personal
    this.orderModel =[{},'','','','','','','',''];
    this.menuModel = [{desc: 'menuId', value:'1'},{desc:'name',value:'Asian Buffet'},
    {desc:'desc',value:'Asian Food'},{desc:'price',value:19},{desc:'unit',value:'pax'}];
    
    
  }

  submitOrder() {

  }

}
