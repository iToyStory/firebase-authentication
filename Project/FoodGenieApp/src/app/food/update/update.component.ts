import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuService } from '../../shared/services/menu.service';
import { BuffetSet } from '../../shared/models/buffet-set';
import { AlacartDish } from '../../shared/models/alacart-dish';
import { DatePipe } from '@angular/common';
import { BuffetDishDetails } from '../../shared/models/buffet-dish-details';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})

export class UpdateMenuComponent implements OnInit {

  modalRef: BsModalRef; // modal 
// **** HIDE ALL WHEN PAGE LOAD ****  
  showMenus: boolean = false;      // buffet set menu 
  showTypes: boolean=false;        // checkbox for dish type 
  // show alacart dish by types
  showAppetisers: boolean = false; 
  showStaples: boolean = false;
  showSeafoods: boolean = false;
  showFishes: boolean = false;
  showRedmeats: boolean = false;
  showChickens: boolean = false;
  showVegetables: boolean = false;
  showSides: boolean = false;
  showBakes: boolean = false;
  showDesserts: boolean = false;
  showBeverages: boolean = false;
  itemSelected: boolean = false;

// **** FOR LOADING ****
  buffets: BuffetSet[]; // for loading, store buffet_name, descriptions, price, unit, isObsolete, menuId
  menuItems : string[] = [];  // store buffet details, dish_name, descriptions, vegetarian
  dishTypes =[];  // store type_id, type, dish_rank
  // store dish_id, dish_name, descriptions, price, unit, dish_rank, vegetarian, updated_by
  appetisers = [];
  staples = [];
  seafoods = [];
  fishes = [];
  redmeats = [];
  chickens = [];
  vegetables = [];
  sides = [];
  bakes = [];
  desserts = [];
  beverages = [];
 /* 
  testItems = [
    {
  "dish_name": "yellow rice",
  "descriptions": "this is malay rice",
  "vegetarian": "y"
  },
    {
  "dish_name": "sugarcane prawn",
  "descriptions": "this is thai prawn",
  "vegetarian": "n"
  },
    {
  "dish_name": "calamari ring",
  "descriptions": "this is western seafood",
  "vegetarian": "n"
  },
    {
  "dish_name": "teriyaki salmon",
  "descriptions": "this is japanese fish",
  "vegetarian": "n"
  },
    {
  "dish_name": "salsa chicken",
  "descriptions": "this is western chicken",
  "vegetarian": "n"
  },
    {
  "dish_name": "capsicum omelete",
  "descriptions": "this is western omelete",
  "vegetarian": "n"
  },
    {
  "dish_name": "potato croquette",
  "descriptions": "this is japanese potato",
  "vegetarian": "n"
  },
    {
  "dish_name": "fruit tartlet",
  "descriptions": "this is western bakery",
  "vegetarian": "n"
  },
    {
  "dish_name": "panna cotta",
  "descriptions": "this is western dessert",
  "vegetarian": "n"
  },
    {
  "dish_name": "orange juice",
  "descriptions": "this is western dessert",
  "vegetarian": "y"
  }
  ]*/

// **** FOR EDIT, SAVE & PUT ****
  newMenu: BuffetSet; // for saveNewMenu()
  editBuffet: BuffetSet; // for editMenuForm, viewMenu(), 
  arrayIndex: number; // array location of the selected menu
  unitOptions = [ {desc:'per Pax', value:'pax'}, {desc: 'per Piece', value:'pc'}, {desc:'per Tray', value:'tray'}];

// **** FOR ADDING NEW ITEMS ****
  newMenuDishList : string[] =[] // for addToList(), store dish_name of selected items
  newMenuDishId: number [] = []  // for addToList(), store dish_id of selected items, for passing to server
  newDish: AlacartDish; // for newDishForm, addAdish()
  insertId: number;

// **** FOR DELETE ****
canDelete: boolean = true; // false if menu is in use, then disable delete button
orders = [];   // loadOrderForMenu(), store order_number, event_date

  constructor(
    private modalService: BsModalService,
    private menuService: MenuService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadMenus();
    this.loadDishType();
    this.loadAlacart();
}

// **** TOGGLE SHOW / HIDE ****
listMenus() {
  //console.log("show menus")
  this.showMenus = true;   // show buffet set menus
// hide the rest
  this.showTypes = false;
  this.showAppetisers = false;
  this.showStaples = false;
  this.showSeafoods = false;
  this.showFishes = false;
  this.showRedmeats = false;
  this.showChickens = false;
  this.showVegetables = false;
  this.showSides = false;
  this.showBakes = false;
  this.showDesserts = false;
  this.showBeverages = false;
}
// **** TOGGLE SHOW / HIDE **** 
createMenu() {  
  this.showMenus = false;     // hide
  this.showTypes = true;      // checkbox for dish type
  // show all alacart dish
  this.showAppetisers = true;   
  this.showStaples = true;
  this.showSeafoods = true;
  this.showFishes = true;
  this.showRedmeats = true;
  this.showChickens = true;
  this.showVegetables = true;
  this.showSides = true;
  this.showBakes = true;
  this.showDesserts = true;
  this.showBeverages = true;
}

// **** FOR LOADING ****
loadMenus() {
  this.menuService.getAllMenus()
 //   .then((data) => console.log(data)) // print return data
      .then((data) => {this.buffets=data})  // store menu title (buffet_name, descriptions, price, unit, isObsolete, menuId)
      .catch((error) => { console.log(error); })
 // console.log("load menus")
}
// **** FOR LOADING ****
loadMenuDetails(id,index) {
  this.menuService.getMenuDetails(id)
  //  .then((data) => console.log(data)) // print return data
    .then((data) => {this.menuItems=data})  // store buffet details (dish_name, descriptions, vegetarian)
    .catch((error) => { console.log(error); })
//  console.log("load menus details")
}
// **** FOR LOADING ****
loadDishType() {
  this.menuService.getDishType()
  .then((data) => {this.dishTypes=data})  // store dish types (type_id, type, dish_rank)
  .catch((error) => { console.log(error); })
//  console.log("load dish types")
}
// **** FOR LOADING ****
// sort dishes according to dish rank (which is derived from dish type)
loadAlacart() {
 // console.log("load alacart")
  this.menuService.getAlacart()
   //  .then((data:any) => console.log(data)) // print return data
     .then((data) => {
      for ( let i=0; i< data.length; i++ ) { // length of array
        switch(data[i].dish_rank) {          // compare with the dish rank of this location
          case 1 : {
            this.appetisers.push(data[i]); 
            break;
          }
          case 2 : {
            this.staples.push(data[i]);
            break;
          }
          case 3 : {
            this.seafoods.push(data[i]);
            break;
          }
          case 4 : {
            this.fishes.push(data[i]);
            break;
          }
          case 5 : {
            this.redmeats.push(data[i]);
            break;
          }
          case 6 : {
            this.chickens.push(data[i]);
            break;
          }
          case 7 : {
            this.vegetables.push(data[i]);
            break;
          }
          case 8 : {
            this.sides.push(data[i]);
            break;
          }
          case 9 : {
            this.bakes.push(data[i]);
            break;
          }
          case 10 : {
            this.desserts.push(data[i]);
            break;
          }
          case 11 : {
            this.beverages.push(data[i]);
            break;
          }
        } // switch
      } // for
    }) 
  .catch((error) => { console.log(error); });
} // load alacart

// **** FOR EDIT, SAVE & PUT ****
// >> allow to change dishes if menu is not in use? (not implement yet)
viewMenu(selectedMenu: BuffetSet,template: TemplateRef<any>, index) {
  this.loadMenuDetails(selectedMenu.buffet_id,index)
  this.editBuffet = new BuffetSet( 
    selectedMenu.buffet_name,   // follows the names defined on model class
    selectedMenu.descriptions, 
    selectedMenu.price,
    selectedMenu.unit,
    selectedMenu.updated_by,
    selectedMenu.is_obsolete,
    selectedMenu.buffet_id,
);
this.arrayIndex =index, // index location of the selected menu
//console.log("buffet_id")
console.log(selectedMenu.buffet_id)
this.loadOrderForMenu(selectedMenu.buffet_id)     // check if menu is in use
this.modalRef = this.modalService.show(template); // display the template on screen
}
// **** FOR EDIT, SAVE & PUT ****
saveEditMenu() {
this.modalRef.hide(); // close the template
this.menuService.updateOneMenu(this.editBuffet)    // send updated menu title to server
  .then(() => {alert('save menu update');})                 // change this later
  .catch((error) => { console.log('Error:',error) })
  this.loadMenus()
} 

// **** FUNCTIONS FOR CREATE NEW MENU / DISH  ****
// <updated_by> default value 1, <is_obsolete> default value 0
// >> how to pre-select veg based on dish type?
CreateNewDish(selectedDish:AlacartDish,template: TemplateRef<any>,rank) {
  this.newDish = new AlacartDish(rank,'','',null,'pax',selectedDish.vegetarian,1,0) 
  this.modalRef = this.modalService.show(template); // display the template on screen
}
// **** FUNCTIONS FOR CREATE NEW DISH  ****
//  Adding A New Alacart
// >> menu did not update upon save
saveNewDish(){
  console.log(this.newDish)
  this.menuService.addOneAlacart(this.newDish)    // send new dish to server
  .then(() => {alert('dish saved');})
  .catch((error) => { console.log('Error:',error) })
  this.modalRef.hide(); // close the template
}
// **** FUNCTIONS FOR CREATE NEW MENU  ****
addToList(item) {
  this.newMenuDishList.push(item.dish_name);      // store selected items (dish_name)
  this.newMenuDishId.push(item.dish_id)
  console.log(this.newMenuDishList);
  console.log(this.newMenuDishId);
  this.itemSelected =true;
}
// **** FUNCTIONS FOR CREATE NEW MENU  ****
// remove a dish from the selected list
removeFromList(index){
this.newMenuDishList.splice(index,1)  // remove 1 item from index position
if (this.newMenuDishList.length==0)
  this.itemSelected =false;
}
// **** FUNCTIONS FOR CREATE NEW MENU  ****
editNewMenu(template: TemplateRef<any>) {
  this.newMenu = new BuffetSet('','',null,'pax',1,0)
  this.modalRef = this.modalService.show(template); // display the template on screen
}
// **** FUNCTIONS FOR CREATE NEW MENU  ****
saveNewMenu() {
  this.menuService.addOneMenu(this.newMenu)    // step 1. post menu title to server
  .then((data) =>  {                            // data returns insertId
    this.saveMenuDetails(data);                 // proceed to step 2. save menu details 
  })       
  .catch((error) => { console.log('Error:',error) })
  this.modalRef.hide();                  // close the template
}

// step 2. save menu details to server
saveMenuDetails(insertId){
  for (let i=0; i < this.newMenuDishId.length; i++) {
    console.log(this.newMenuDishId[i])
      this.menuService.addMenuDetails(insertId,this.newMenuDishId[i])   
      .then(() =>  {  } )               
      .catch((error) => { console.log('Error:',error) })    
  } 
  console.log("menu added")
  this.newMenuDishList =[]; // reset array, for next new menu
  this.newMenuDishId =[];
}

// **** FUNCTIONS FOR DELETE ****
// check if menu is in use
loadOrderForMenu(id) {
  this.menuService.getOrderForMenu(id)
  //.then((data) => console.log(data))  // print return data
  .then((data) => {this.orders=data;    // store order_number, event_date
    if (data.length>0) {
      this.canDelete = false; }
      else {
      this.canDelete = true;   
      }  // true if menu is in use, then disable delete button
  })
  .catch((error) => { console.log(error); })
  console.log("can delete")
  console.log(this.canDelete)
}
// **** FUNCTIONS FOR DELETE ****
// >> check if dish is in use (not implement yet)
DeleteOneAlacart(id) {
  console.log("delete this")
  this.menuService.deleteOneDish(id)              // delete dish from server
  .then(() => {alert('deleted');})                 // change this later
  .catch((error) => { console.log('Error:',error) })
}
// **** FUNCTIONS FOR DELETE ****
// delete is allowed if menu is not in use 
// >> alternative is soft-delete  (not yet implement)
removeMenuDetails(id) {
  this.menuService.deleteMenuDetails(id)       // step 1, delete buffet details at server
  .then(()=> {         
    this.removeMenu(id)                        // proceed to step 2, delete menu title
  })                      
  .catch((error) => {console.log('Error:', error)})                           
}
// **** FUNCTIONS FOR DELETE ****
removeMenu(id) {
  this.menuService.deleteOneMenu(id)           // step 2, delete menu title on server
  .then(() => {alert('menu deleted');})             // change this later
  .catch((error) => { console.log('Error:',error) })
  this.modalRef.hide();                  // close the template
}


// ***** FUNCTIONS FOR NEW USER ****
createNewUser() {
  this.showMenus = false;     // hide
  this.showTypes = false;      // checkbox for dish type
}















// **** FOR TESTING ONLY ***
// from test data assets/data/menus.json
loadMenus1() {
  this.menuService.getAllMenus1() 
      .subscribe((data:any) => this.buffets=data)
//   .subscribe((data:any) => console.log(data)) // print return data
}

loadDishType1() {
  this.menuService.getDishType1()
  .subscribe((data:any) => this.dishTypes=data)
  //   .subscribe((data:any) => console.log(data)) // print return data
}

// load alacart dish from test data assets/data/alacartData.json
// sort dishes according to dish rank (dish rank is same as dish type)
loadAlacart1() {
  this.menuService.getAlacart1()
     .subscribe(data => {
      for ( let i=0; i< data.length; i++ ) { // length of array
        switch(data[i].dish_rank) {  // compare with the dish rank of this location
          case  1 : {
            this.appetisers.push(data[i]); 
            break;
          }
          case 2 : {
            this.staples.push(data[i]);
            break;
          }
          case 3 : {
            this.seafoods.push(data[i]);
            break;
          }
          case 4 : {
            this.fishes.push(data[i]);
            break;
          }
          case 5 : {
            this.redmeats.push(data[i]);
            break;
          }
          case 6 : {
            this.chickens.push(data[i]);
            break;
          }
          case 7 : {
            this.vegetables.push(data[i]);
            break;
          }
          case 8 : {
            this.sides.push(data[i]);
            break;
          }
          case 9 : {
            this.bakes.push(data[i]);
            break;
          }
          case 10 : {
            this.desserts.push(data[i]);
            break;
          }
          case 11 : {
            this.beverages.push(data[i]);
            break;
          }
        } // switch
      } // for
    })
  // .subscribe((data:any) => console.log(data[0].dish_rank)) // print return data
  } // end

  // build new menu from an exiting menu
// not implement yet
mirrorAmenu() {

}

} //Onit

  
