import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap';
import { CollapseModule } from 'ngx-bootstrap';
import { PopoverModule } from 'ngx-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap';
import { PaginationModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap';
import { CarouselModule } from 'ngx-bootstrap';
import { SearchComponent } from './search/search.component';
import { UpdateMenuComponent } from './update/update.component';
import { OrderComponent } from './order/order.component';
import { MenuComponent } from './menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

const booksRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'menu',
    component: MenuComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'update',
    component: UpdateMenuComponent
  },
  {
    path: 'order',
    component: OrderComponent
  }
]);

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    booksRouting,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    BrowserAnimationsModule,
    PaginationModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [OrderComponent, UpdateMenuComponent, SearchComponent, MenuComponent]
})
export class FoodModule { }