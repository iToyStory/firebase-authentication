import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { ToastyModule } from 'ng2-toasty';


import {
  FooterComponent,
  HeaderComponent,
  OrderService,
  MenuService,
  SharedModule
} from './shared';

import {
  FoodModule
} from './food';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([]);

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,    
  ],
  imports: [
    BrowserModule,
    HomeModule,
    rootRouting,
    SharedModule,
    FoodModule,
    ToastyModule
  ],
  providers: [MenuService, OrderService],
  bootstrap: [AppComponent]
})

export class AppModule { }
